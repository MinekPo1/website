templates = $(wildcard src/**.template.pug)
pages = $(filter-out $(templates),$(wildcard src/**.pug))
styles = $(wildcard src/**.sass)

template_o = $(patsubst src/%.pug,public/%.js,$(templates))
page_o = $(patsubst src/%.pug,public/%.html,$(pages))
style_o = $(patsubst src/%.sass,public/%.css,$(styles))

public:
	mkdir public

public/%.template.js: public src/%.template.pug
	npx pug -c $^ -o $(dir $@)

public/%.html: public src/%.pug
	npx pug $^ -o $(dir $@)

public/%.css: public src/%.sass
	npx sass $^:$@

.PHONY: site

site: public $(template_o) $(page_o) $(style_o)
ifneq ($(wildcard res),)
	cp -r res public/res
endif

.PHONY: clean
clean:
	rm -rf public
